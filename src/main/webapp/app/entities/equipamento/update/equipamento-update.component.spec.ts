jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { EquipamentoService } from '../service/equipamento.service';
import { IEquipamento, Equipamento } from '../equipamento.model';
import { IResponsavel } from 'app/entities/responsavel/responsavel.model';
import { ResponsavelService } from 'app/entities/responsavel/service/responsavel.service';

import { EquipamentoUpdateComponent } from './equipamento-update.component';

describe('Equipamento Management Update Component', () => {
  let comp: EquipamentoUpdateComponent;
  let fixture: ComponentFixture<EquipamentoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let equipamentoService: EquipamentoService;
  let responsavelService: ResponsavelService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [EquipamentoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(EquipamentoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EquipamentoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    equipamentoService = TestBed.inject(EquipamentoService);
    responsavelService = TestBed.inject(ResponsavelService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call responsavel query and add missing value', () => {
      const equipamento: IEquipamento = { id: 456 };
      const responsavel: IResponsavel = { id: 99732 };
      equipamento.responsavel = responsavel;

      const responsavelCollection: IResponsavel[] = [{ id: 58834 }];
      jest.spyOn(responsavelService, 'query').mockReturnValue(of(new HttpResponse({ body: responsavelCollection })));
      const expectedCollection: IResponsavel[] = [responsavel, ...responsavelCollection];
      jest.spyOn(responsavelService, 'addResponsavelToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ equipamento });
      comp.ngOnInit();

      expect(responsavelService.query).toHaveBeenCalled();
      expect(responsavelService.addResponsavelToCollectionIfMissing).toHaveBeenCalledWith(responsavelCollection, responsavel);
      expect(comp.responsavelsCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const equipamento: IEquipamento = { id: 456 };
      const responsavel: IResponsavel = { id: 1176 };
      equipamento.responsavel = responsavel;

      activatedRoute.data = of({ equipamento });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(equipamento));
      expect(comp.responsavelsCollection).toContain(responsavel);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Equipamento>>();
      const equipamento = { id: 123 };
      jest.spyOn(equipamentoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ equipamento });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: equipamento }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(equipamentoService.update).toHaveBeenCalledWith(equipamento);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Equipamento>>();
      const equipamento = new Equipamento();
      jest.spyOn(equipamentoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ equipamento });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: equipamento }));
      saveSubject.complete();

      // THEN
      expect(equipamentoService.create).toHaveBeenCalledWith(equipamento);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Equipamento>>();
      const equipamento = { id: 123 };
      jest.spyOn(equipamentoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ equipamento });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(equipamentoService.update).toHaveBeenCalledWith(equipamento);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackResponsavelById', () => {
      it('Should return tracked Responsavel primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackResponsavelById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
