import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { EquipamentoComponent } from '../list/equipamento.component';
import { EquipamentoDetailComponent } from '../detail/equipamento-detail.component';
import { EquipamentoUpdateComponent } from '../update/equipamento-update.component';
import { EquipamentoRoutingResolveService } from './equipamento-routing-resolve.service';

const equipamentoRoute: Routes = [
  {
    path: '',
    component: EquipamentoComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EquipamentoDetailComponent,
    resolve: {
      equipamento: EquipamentoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EquipamentoUpdateComponent,
    resolve: {
      equipamento: EquipamentoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EquipamentoUpdateComponent,
    resolve: {
      equipamento: EquipamentoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(equipamentoRoute)],
  exports: [RouterModule],
})
export class EquipamentoRoutingModule {}
