import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEquipamento } from '../equipamento.model';

@Component({
  selector: 'jhi-equipamento-detail',
  templateUrl: './equipamento-detail.component.html',
})
export class EquipamentoDetailComponent implements OnInit {
  equipamento: IEquipamento | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ equipamento }) => {
      this.equipamento = equipamento;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
