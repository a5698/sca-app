import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEquipamento } from '../equipamento.model';
import { EquipamentoService } from '../service/equipamento.service';
import { EquipamentoDeleteDialogComponent } from '../delete/equipamento-delete-dialog.component';

@Component({
  selector: 'jhi-equipamento',
  templateUrl: './equipamento.component.html',
})
export class EquipamentoComponent implements OnInit {
  equipamentos?: IEquipamento[];
  isLoading = false;

  constructor(protected equipamentoService: EquipamentoService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.equipamentoService.query().subscribe(
      (res: HttpResponse<IEquipamento[]>) => {
        this.isLoading = false;
        this.equipamentos = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IEquipamento): number {
    return item.id!;
  }

  delete(equipamento: IEquipamento): void {
    const modalRef = this.modalService.open(EquipamentoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.equipamento = equipamento;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
