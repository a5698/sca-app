import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { EquipamentoService } from '../service/equipamento.service';

import { EquipamentoComponent } from './equipamento.component';

describe('Equipamento Management Component', () => {
  let comp: EquipamentoComponent;
  let fixture: ComponentFixture<EquipamentoComponent>;
  let service: EquipamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [EquipamentoComponent],
    })
      .overrideTemplate(EquipamentoComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EquipamentoComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(EquipamentoService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.equipamentos?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
