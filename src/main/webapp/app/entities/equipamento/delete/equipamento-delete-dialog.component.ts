import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEquipamento } from '../equipamento.model';
import { EquipamentoService } from '../service/equipamento.service';

@Component({
  templateUrl: './equipamento-delete-dialog.component.html',
})
export class EquipamentoDeleteDialogComponent {
  equipamento?: IEquipamento;

  constructor(protected equipamentoService: EquipamentoService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.equipamentoService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
