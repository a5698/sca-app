import { IResponsavel } from 'app/entities/responsavel/responsavel.model';
import { TipoInsumo } from 'app/entities/enumerations/tipo-insumo.model';

export interface IInsumo {
  id?: number;
  codInsumo?: string;
  tipoInsumo?: TipoInsumo;
  fabricante?: string;
  modelo?: string;
  responsavel?: IResponsavel | null;
}

export class Insumo implements IInsumo {
  constructor(
    public id?: number,
    public codInsumo?: string,
    public tipoInsumo?: TipoInsumo,
    public fabricante?: string,
    public modelo?: string,
    public responsavel?: IResponsavel | null
  ) {}
}

export function getInsumoIdentifier(insumo: IInsumo): number | undefined {
  return insumo.id;
}
