jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { InsumoService } from '../service/insumo.service';
import { IInsumo, Insumo } from '../insumo.model';
import { IResponsavel } from 'app/entities/responsavel/responsavel.model';
import { ResponsavelService } from 'app/entities/responsavel/service/responsavel.service';

import { InsumoUpdateComponent } from './insumo-update.component';

describe('Insumo Management Update Component', () => {
  let comp: InsumoUpdateComponent;
  let fixture: ComponentFixture<InsumoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let insumoService: InsumoService;
  let responsavelService: ResponsavelService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [InsumoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(InsumoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(InsumoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    insumoService = TestBed.inject(InsumoService);
    responsavelService = TestBed.inject(ResponsavelService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call responsavel query and add missing value', () => {
      const insumo: IInsumo = { id: 456 };
      const responsavel: IResponsavel = { id: 43519 };
      insumo.responsavel = responsavel;

      const responsavelCollection: IResponsavel[] = [{ id: 57202 }];
      jest.spyOn(responsavelService, 'query').mockReturnValue(of(new HttpResponse({ body: responsavelCollection })));
      const expectedCollection: IResponsavel[] = [responsavel, ...responsavelCollection];
      jest.spyOn(responsavelService, 'addResponsavelToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ insumo });
      comp.ngOnInit();

      expect(responsavelService.query).toHaveBeenCalled();
      expect(responsavelService.addResponsavelToCollectionIfMissing).toHaveBeenCalledWith(responsavelCollection, responsavel);
      expect(comp.responsavelsCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const insumo: IInsumo = { id: 456 };
      const responsavel: IResponsavel = { id: 47080 };
      insumo.responsavel = responsavel;

      activatedRoute.data = of({ insumo });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(insumo));
      expect(comp.responsavelsCollection).toContain(responsavel);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Insumo>>();
      const insumo = { id: 123 };
      jest.spyOn(insumoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ insumo });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: insumo }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(insumoService.update).toHaveBeenCalledWith(insumo);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Insumo>>();
      const insumo = new Insumo();
      jest.spyOn(insumoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ insumo });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: insumo }));
      saveSubject.complete();

      // THEN
      expect(insumoService.create).toHaveBeenCalledWith(insumo);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Insumo>>();
      const insumo = { id: 123 };
      jest.spyOn(insumoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ insumo });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(insumoService.update).toHaveBeenCalledWith(insumo);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackResponsavelById', () => {
      it('Should return tracked Responsavel primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackResponsavelById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
