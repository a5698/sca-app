import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IInsumo, Insumo } from '../insumo.model';
import { InsumoService } from '../service/insumo.service';
import { IResponsavel } from 'app/entities/responsavel/responsavel.model';
import { ResponsavelService } from 'app/entities/responsavel/service/responsavel.service';
import { TipoInsumo } from 'app/entities/enumerations/tipo-insumo.model';

@Component({
  selector: 'jhi-insumo-update',
  templateUrl: './insumo-update.component.html',
})
export class InsumoUpdateComponent implements OnInit {
  isSaving = false;
  tipoInsumoValues = Object.keys(TipoInsumo);

  responsavelsCollection: IResponsavel[] = [];

  editForm = this.fb.group({
    id: [],
    codInsumo: [null, [Validators.required]],
    tipoInsumo: [null, [Validators.required]],
    fabricante: [null, [Validators.required]],
    modelo: [null, [Validators.required]],
    responsavel: [],
  });

  constructor(
    protected insumoService: InsumoService,
    protected responsavelService: ResponsavelService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ insumo }) => {
      this.updateForm(insumo);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const insumo = this.createFromForm();
    if (insumo.id !== undefined) {
      this.subscribeToSaveResponse(this.insumoService.update(insumo));
    } else {
      this.subscribeToSaveResponse(this.insumoService.create(insumo));
    }
  }

  trackResponsavelById(index: number, item: IResponsavel): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInsumo>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(insumo: IInsumo): void {
    this.editForm.patchValue({
      id: insumo.id,
      codInsumo: insumo.codInsumo,
      tipoInsumo: insumo.tipoInsumo,
      fabricante: insumo.fabricante,
      modelo: insumo.modelo,
      responsavel: insumo.responsavel,
    });

    this.responsavelsCollection = this.responsavelService.addResponsavelToCollectionIfMissing(
      this.responsavelsCollection,
      insumo.responsavel
    );
  }

  protected loadRelationshipsOptions(): void {
    this.responsavelService
      .query({ filter: 'insumo-is-null' })
      .pipe(map((res: HttpResponse<IResponsavel[]>) => res.body ?? []))
      .pipe(
        map((responsavels: IResponsavel[]) =>
          this.responsavelService.addResponsavelToCollectionIfMissing(responsavels, this.editForm.get('responsavel')!.value)
        )
      )
      .subscribe((responsavels: IResponsavel[]) => (this.responsavelsCollection = responsavels));
  }

  protected createFromForm(): IInsumo {
    return {
      ...new Insumo(),
      id: this.editForm.get(['id'])!.value,
      codInsumo: this.editForm.get(['codInsumo'])!.value,
      tipoInsumo: this.editForm.get(['tipoInsumo'])!.value,
      fabricante: this.editForm.get(['fabricante'])!.value,
      modelo: this.editForm.get(['modelo'])!.value,
      responsavel: this.editForm.get(['responsavel'])!.value,
    };
  }
}
