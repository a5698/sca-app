import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IServico } from '../servico.model';
import { ServicoService } from '../service/servico.service';

@Component({
  templateUrl: './servico-delete-dialog.component.html',
})
export class ServicoDeleteDialogComponent {
  servico?: IServico;

  constructor(protected servicoService: ServicoService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.servicoService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
