import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { TipoServico } from 'app/entities/enumerations/tipo-servico.model';
import { IServico, Servico } from '../servico.model';

import { ServicoService } from './servico.service';

describe('Servico Service', () => {
  let service: ServicoService;
  let httpMock: HttpTestingController;
  let elemDefault: IServico;
  let expectedResult: IServico | IServico[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ServicoService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      codServico: 'AAAAAAA',
      tipoServico: TipoServico.MANUTENCAO_PREVENTIVA,
      descricao: 'AAAAAAA',
      dataServico: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dataServico: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Servico', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dataServico: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dataServico: currentDate,
        },
        returnedFromService
      );

      service.create(new Servico()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Servico', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          codServico: 'BBBBBB',
          tipoServico: 'BBBBBB',
          descricao: 'BBBBBB',
          dataServico: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dataServico: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Servico', () => {
      const patchObject = Object.assign(
        {
          codServico: 'BBBBBB',
          tipoServico: 'BBBBBB',
          descricao: 'BBBBBB',
          dataServico: currentDate.format(DATE_FORMAT),
        },
        new Servico()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dataServico: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Servico', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          codServico: 'BBBBBB',
          tipoServico: 'BBBBBB',
          descricao: 'BBBBBB',
          dataServico: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dataServico: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Servico', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addServicoToCollectionIfMissing', () => {
      it('should add a Servico to an empty array', () => {
        const servico: IServico = { id: 123 };
        expectedResult = service.addServicoToCollectionIfMissing([], servico);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(servico);
      });

      it('should not add a Servico to an array that contains it', () => {
        const servico: IServico = { id: 123 };
        const servicoCollection: IServico[] = [
          {
            ...servico,
          },
          { id: 456 },
        ];
        expectedResult = service.addServicoToCollectionIfMissing(servicoCollection, servico);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Servico to an array that doesn't contain it", () => {
        const servico: IServico = { id: 123 };
        const servicoCollection: IServico[] = [{ id: 456 }];
        expectedResult = service.addServicoToCollectionIfMissing(servicoCollection, servico);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(servico);
      });

      it('should add only unique Servico to an array', () => {
        const servicoArray: IServico[] = [{ id: 123 }, { id: 456 }, { id: 6651 }];
        const servicoCollection: IServico[] = [{ id: 123 }];
        expectedResult = service.addServicoToCollectionIfMissing(servicoCollection, ...servicoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const servico: IServico = { id: 123 };
        const servico2: IServico = { id: 456 };
        expectedResult = service.addServicoToCollectionIfMissing([], servico, servico2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(servico);
        expect(expectedResult).toContain(servico2);
      });

      it('should accept null and undefined values', () => {
        const servico: IServico = { id: 123 };
        expectedResult = service.addServicoToCollectionIfMissing([], null, servico, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(servico);
      });

      it('should return initial array if no Servico is added', () => {
        const servicoCollection: IServico[] = [{ id: 123 }];
        expectedResult = service.addServicoToCollectionIfMissing(servicoCollection, undefined, null);
        expect(expectedResult).toEqual(servicoCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
