jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ServicoService } from '../service/servico.service';
import { IServico, Servico } from '../servico.model';
import { IEquipamento } from 'app/entities/equipamento/equipamento.model';
import { EquipamentoService } from 'app/entities/equipamento/service/equipamento.service';

import { ServicoUpdateComponent } from './servico-update.component';

describe('Servico Management Update Component', () => {
  let comp: ServicoUpdateComponent;
  let fixture: ComponentFixture<ServicoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let servicoService: ServicoService;
  let equipamentoService: EquipamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ServicoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(ServicoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ServicoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    servicoService = TestBed.inject(ServicoService);
    equipamentoService = TestBed.inject(EquipamentoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Equipamento query and add missing value', () => {
      const servico: IServico = { id: 456 };
      const equipamento: IEquipamento = { id: 10317 };
      servico.equipamento = equipamento;

      const equipamentoCollection: IEquipamento[] = [{ id: 23211 }];
      jest.spyOn(equipamentoService, 'query').mockReturnValue(of(new HttpResponse({ body: equipamentoCollection })));
      const additionalEquipamentos = [equipamento];
      const expectedCollection: IEquipamento[] = [...additionalEquipamentos, ...equipamentoCollection];
      jest.spyOn(equipamentoService, 'addEquipamentoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ servico });
      comp.ngOnInit();

      expect(equipamentoService.query).toHaveBeenCalled();
      expect(equipamentoService.addEquipamentoToCollectionIfMissing).toHaveBeenCalledWith(equipamentoCollection, ...additionalEquipamentos);
      expect(comp.equipamentosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const servico: IServico = { id: 456 };
      const equipamento: IEquipamento = { id: 97704 };
      servico.equipamento = equipamento;

      activatedRoute.data = of({ servico });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(servico));
      expect(comp.equipamentosSharedCollection).toContain(equipamento);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Servico>>();
      const servico = { id: 123 };
      jest.spyOn(servicoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ servico });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: servico }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(servicoService.update).toHaveBeenCalledWith(servico);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Servico>>();
      const servico = new Servico();
      jest.spyOn(servicoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ servico });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: servico }));
      saveSubject.complete();

      // THEN
      expect(servicoService.create).toHaveBeenCalledWith(servico);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Servico>>();
      const servico = { id: 123 };
      jest.spyOn(servicoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ servico });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(servicoService.update).toHaveBeenCalledWith(servico);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackEquipamentoById', () => {
      it('Should return tracked Equipamento primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackEquipamentoById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
