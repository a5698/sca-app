import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IServico, Servico } from '../servico.model';
import { ServicoService } from '../service/servico.service';
import { IEquipamento } from 'app/entities/equipamento/equipamento.model';
import { EquipamentoService } from 'app/entities/equipamento/service/equipamento.service';
import { TipoServico } from 'app/entities/enumerations/tipo-servico.model';

@Component({
  selector: 'jhi-servico-update',
  templateUrl: './servico-update.component.html',
})
export class ServicoUpdateComponent implements OnInit {
  isSaving = false;
  tipoServicoValues = Object.keys(TipoServico);

  equipamentosSharedCollection: IEquipamento[] = [];

  editForm = this.fb.group({
    id: [],
    codServico: [null, [Validators.required]],
    tipoServico: [null, [Validators.required]],
    descricao: [null, [Validators.required]],
    dataServico: [null, [Validators.required]],
    equipamento: [],
  });

  constructor(
    protected servicoService: ServicoService,
    protected equipamentoService: EquipamentoService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ servico }) => {
      this.updateForm(servico);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const servico = this.createFromForm();
    if (servico.id !== undefined) {
      this.subscribeToSaveResponse(this.servicoService.update(servico));
    } else {
      this.subscribeToSaveResponse(this.servicoService.create(servico));
    }
  }

  trackEquipamentoById(index: number, item: IEquipamento): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IServico>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(servico: IServico): void {
    this.editForm.patchValue({
      id: servico.id,
      codServico: servico.codServico,
      tipoServico: servico.tipoServico,
      descricao: servico.descricao,
      dataServico: servico.dataServico,
      equipamento: servico.equipamento,
    });

    this.equipamentosSharedCollection = this.equipamentoService.addEquipamentoToCollectionIfMissing(
      this.equipamentosSharedCollection,
      servico.equipamento
    );
  }

  protected loadRelationshipsOptions(): void {
    this.equipamentoService
      .query()
      .pipe(map((res: HttpResponse<IEquipamento[]>) => res.body ?? []))
      .pipe(
        map((equipamentos: IEquipamento[]) =>
          this.equipamentoService.addEquipamentoToCollectionIfMissing(equipamentos, this.editForm.get('equipamento')!.value)
        )
      )
      .subscribe((equipamentos: IEquipamento[]) => (this.equipamentosSharedCollection = equipamentos));
  }

  protected createFromForm(): IServico {
    return {
      ...new Servico(),
      id: this.editForm.get(['id'])!.value,
      codServico: this.editForm.get(['codServico'])!.value,
      tipoServico: this.editForm.get(['tipoServico'])!.value,
      descricao: this.editForm.get(['descricao'])!.value,
      dataServico: this.editForm.get(['dataServico'])!.value,
      equipamento: this.editForm.get(['equipamento'])!.value,
    };
  }
}
