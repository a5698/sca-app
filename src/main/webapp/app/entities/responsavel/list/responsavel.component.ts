import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IResponsavel } from '../responsavel.model';
import { ResponsavelService } from '../service/responsavel.service';
import { ResponsavelDeleteDialogComponent } from '../delete/responsavel-delete-dialog.component';

@Component({
  selector: 'jhi-responsavel',
  templateUrl: './responsavel.component.html',
})
export class ResponsavelComponent implements OnInit {
  responsavels?: IResponsavel[];
  isLoading = false;

  constructor(protected responsavelService: ResponsavelService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.responsavelService.query().subscribe(
      (res: HttpResponse<IResponsavel[]>) => {
        this.isLoading = false;
        this.responsavels = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IResponsavel): number {
    return item.id!;
  }

  delete(responsavel: IResponsavel): void {
    const modalRef = this.modalService.open(ResponsavelDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.responsavel = responsavel;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
