import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'equipamento',
        data: { pageTitle: 'madmApp.equipamento.home.title' },
        loadChildren: () => import('./equipamento/equipamento.module').then(m => m.EquipamentoModule),
      },
      {
        path: 'insumo',
        data: { pageTitle: 'madmApp.insumo.home.title' },
        loadChildren: () => import('./insumo/insumo.module').then(m => m.InsumoModule),
      },
      {
        path: 'responsavel',
        data: { pageTitle: 'madmApp.responsavel.home.title' },
        loadChildren: () => import('./responsavel/responsavel.module').then(m => m.ResponsavelModule),
      },
      {
        path: 'servico',
        data: { pageTitle: 'madmApp.servico.home.title' },
        loadChildren: () => import('./servico/servico.module').then(m => m.ServicoModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
